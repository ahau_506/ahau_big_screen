/******
 * @ Author: qiaoyu
 * @ Date: 2022-03-26 19:01:20
* @ LastEditTime: 2022-05-05 19:20:18
* @ LastEditors: qiaoyu
 * @ Description:
* @ FilePath: \jetlinks_big_screen\js\index.js
 * @ Made by qiaoyukeji@gmail.com
 */

// 时间戳转时间
function set_time(str) {
  var n = parseInt(str);
  var D = new Date(n);
  var year = D.getFullYear(); //四位数年份

  var month = D.getMonth() + 1; //月份(0-11),0为一月份
  month = month < 10 ? "0" + month : month;

  var day = D.getDate(); //月的某一天(1-31)
  day = day < 10 ? "0" + day : day;

  var hours = D.getHours(); //小时(0-23)
  hours = hours < 10 ? "0" + hours : hours;

  var minutes = D.getMinutes(); //分钟(0-59)
  minutes = minutes < 10 ? "0" + minutes : minutes;

  var seconds = D.getSeconds(); //秒(0-59)
  seconds = seconds < 10 ? "0" + seconds : seconds;
  // var week = D.getDay();//周几(0-6),0为周日
  // var weekArr = ['周日','周一','周二','周三','周四','周五','周六'];

  // var now_time = year+'-'+month+'-'+day+' '+hours+':'+minutes+':'+seconds;
  var now_time = hours + ":" + minutes + ":" + seconds;

  return now_time;
}

//   ----------------------------------------------------------------
// 第一个折线图，温度折线图
//初始化 echarts.init(dom容器);

var myChart1 = echarts.init(document.getElementById("chart1"));

//指定配置项与数据
var option1 = {
  color: ["#ed3f35"],
  tooltip: {
    trigger: "axis",
  },
  //   网格配置
  grid: {
    left: "0%",
    right: "0%",
    bottom: "3%",
    top: "3%",
    // 是否显示刻度
    containLabel: true,
    show: true, // 显示边框
    borderColor: "#012f4a", // 边框颜色
  },
  xAxis: {
    type: "category",
    data: [
      "Mon",
      "Tue",
      "Wed",
      "Thu",
      "Fri",
      "11",
      "22",
      "33",
      "44",
      "55",
      "66",
      "77",
      "88",
      "99",
      "00",
    ],
  },
  yAxis: {
    type: "value", // 修改分割线
    axisTick: {
      show: false, // 去除刻度
    },
    axisLabel: {
      color: "#dddddd", // 文字颜色
    },
    splitLine: {
      lineStyle: {
        color: "#012f4a", // 分割线颜色
      },
    },
    axisLabel: {
      fontWeight: "bold",
      fontSize: 16,
    },
  },
  series: [
    {
      data: [],
      type: "line",
      smooth: true,
    },
  ],
};
// axios 发起请求获取数据
setInterval(function () {
  console.log("url.sensorURL=" + url.sensorURL);
  axios
    .post(
      "http://42.192.123.52:9200/properties_341623000000011_2022-05/_search?filter_path=hits.hits._source",
      {
        _source: ["createTime", "numberValue"],
        query: {
          bool: {
            must: [{ match: { property: "wendu" } }, { match_all: {} }],
            must_not: [],
            should: [],
          },
        },
        from: 0,
        size: 6,
        sort: [{ createTime: { order: "desc" } }],
        aggs: {},
      }
    )
    .then(
      function (response) {
        a1 = response.data.hits.hits;
        b1 = [];
        c1 = [];
        console.log(a1.length);
        for (i = 0; i < a1.length; i++) {
          b1[i] = a1[a1.length - 1 - i]._source.numberValue;
          c1[i] = set_time(a1[a1.length - 1 - i]._source.createTime);
        }
        console.log(c1);
        option1.series[0].data = b1;
        option1.xAxis.data = c1;
        //把配置项给实例对象
        myChart1.setOption(option1);
      },
      function (err) {
        console.log(err);
      }
    );
}, 5000);

// ----------------------------------------------------------------
// 第二个折线图，湿度折线图
// 第一个折线图，温度折线图
//初始化 echarts.init(dom容器);

var myChart2 = echarts.init(document.getElementById("chart2"));

//指定配置项与数据
var option2 = {
  color: ["#5470c6"],
  tooltip: {
    trigger: "axis",
  },
  //   网格配置
  grid: {
    left: "0%",
    right: "0%",
    bottom: "3%",
    top: "3%",
    // 是否显示刻度
    containLabel: true,
    show: true, // 显示边框
    borderColor: "#012f4a", // 边框颜色
  },
  xAxis: {
    type: "category",
    data: [
      "Mon",
      "Tue",
      "Wed",
      "Thu",
      "Fri",
      "11",
      "22",
      "33",
      "44",
      "55",
      "66",
      "77",
      "88",
      "99",
      "00",
    ],
  },
  yAxis: {
    type: "value", // 修改分割线
    axisTick: {
      show: false, // 去除刻度
    },
    axisLabel: {
      color: "#dddddd", // 文字颜色
    },
    splitLine: {
      lineStyle: {
        color: "#012f4a", // 分割线颜色
      },
    },
    axisLabel: {
      fontWeight: "bold",
      fontSize: 16,
    },
  },
  series: [
    {
      data: [],
      type: "line",
      smooth: true,
    },
  ],
};
// axios 发起请求获取数据
setInterval(function () {
  axios
    .post(
      "http://42.192.123.52:9200/properties_341623000000011_2022-05/_search?filter_path=hits.hits._source",
      {
        _source: ["createTime", "numberValue"],
        query: {
          bool: {
            must: [{ match: { property: "shidu" } }, { match_all: {} }],
            must_not: [],
            should: [],
          },
        },
        from: 0,
        size: 6,
        sort: [{ createTime: { order: "desc" } }],
        aggs: {},
      }
    )
    .then(
      function (response) {
        a2 = response.data.hits.hits;
        b2 = [];
        c2 = [];
        console.log(a2);
        for (i2 = 0; i2 < a2.length; i2++) {
          b2[i2] = a2[a2.length - 1 - i2]._source.numberValue;
          c2[i2] = set_time(a2[a2.length - 1 - i2]._source.createTime);
        }
        console.log(b2);
        option2.series[0].data = b2;
        option2.xAxis.data = c2;
        //把配置项给实例对象
        myChart2.setOption(option2);
      },
      function (err) {
        console.log(err);
      }
    );
}, 5000);

// ----------------------------------------------------------------
// 第三个折线图，光照折线图
//初始化 echarts.init(dom容器);

var myChart3 = echarts.init(document.getElementById("chart3"));

//指定配置项与数据
var option3 = {
  color: ["#fac858"],
  tooltip: {
    trigger: "axis",
  },
  //   网格配置
  grid: {
    left: "0%",
    right: "0%",
    bottom: "3%",
    top: "3%",
    // 是否显示刻度
    containLabel: true,
    show: true, // 显示边框
    borderColor: "#012f4a", // 边框颜色
  },
  xAxis: {
    type: "category",
    data: [
      "Mon",
      "Tue",
      "Wed",
      "Thu",
      "Fri",
      "11",
      "22",
      "33",
      "44",
      "55",
      "66",
      "77",
      "88",
      "99",
      "00",
    ],
  },
  yAxis: {
    type: "value", // 修改分割线
    axisTick: {
      show: false, // 去除刻度
    },
    axisLabel: {
      color: "#dddddd", // 文字颜色
    },
    splitLine: {
      lineStyle: {
        color: "#012f4a", // 分割线颜色
      },
    },
    axisLabel: {
      fontWeight: "bold",
      fontSize: 16,
    },
  },
  series: [
    {
      data: [],
      type: "line",
      smooth: true,
    },
  ],
};
// axios 发起请求获取数据
setInterval(function () {
  axios
    .post(
      "http://42.192.123.52:9200/properties_341623000000011_2022-05/_search?filter_path=hits.hits._source",
      {
        _source: ["createTime", "numberValue"],
        query: {
          bool: {
            must: [{ match: { property: "guangzhao" } }, { match_all: {} }],
            must_not: [],
            should: [],
          },
        },
        from: 0,
        size: 6,
        sort: [{ createTime: { order: "desc" } }],
        aggs: {},
      }
    )
    .then(
      function (response) {
        a3 = response.data.hits.hits;
        b3 = [];
        c3 = [];
        console.log(a3);
        for (i3 = 0; i3 < a3.length; i3++) {
          b3[i3] = a3[a3.length - 1 - i3]._source.numberValue;
          c3[i3] = set_time(a3[a3.length - 1 - i3]._source.createTime);
        }
        console.log(b3);
        option3.series[0].data = b3;
        option3.xAxis.data = c3;
        //把配置项给实例对象
        myChart3.setOption(option3);
      },
      function (err) {
        console.log(err);
      }
    );
}, 5000);

// ----------------------------------------------------------------
// 第四个折线图，CO2折线图
//初始化 echarts.init(dom容器);

var myChart4 = echarts.init(document.getElementById("chart4"));

//指定配置项与数据
var option4 = {
  color: ["#ee6666"],
  tooltip: {
    trigger: "axis",
  },
  //   网格配置
  grid: {
    left: "0%",
    right: "0%",
    bottom: "3%",
    top: "3%",
    // 是否显示刻度
    containLabel: true,
    show: true, // 显示边框
    borderColor: "#012f4a", // 边框颜色
  },
  xAxis: {
    type: "category",
    data: [
      "Mon",
      "Tue",
      "Wed",
      "Thu",
      "Fri",
      "11",
      "22",
      "33",
      "44",
      "55",
      "66",
      "77",
      "88",
      "99",
      "00",
    ],
  },
  yAxis: {
    type: "value", // 修改分割线
    axisTick: {
      show: false, // 去除刻度
    },
    axisLabel: {
      color: "#dddddd", // 文字颜色
    },
    splitLine: {
      lineStyle: {
        color: "#012f4a", // 分割线颜色
      },
    },
    axisLabel: {
      fontWeight: "bold",
      fontSize: 16,
    },
  },
  series: [
    {
      data: [],
      type: "line",
      smooth: true,
    },
  ],
};
// axios 发起请求获取数据
setInterval(function () {
  axios
    .post(
      "http://42.192.123.52:9200/properties_341623000000011_2022-05/_search?filter_path=hits.hits._source",
      {
        _source: ["createTime", "numberValue"],
        query: {
          bool: {
            must: [{ match: { property: "co2" } }, { match_all: {} }],
            must_not: [],
            should: [],
          },
        },
        from: 0,
        size: 6,
        sort: [{ createTime: { order: "desc" } }],
        aggs: {},
      }
    )
    .then(
      function (response) {
        a4 = response.data.hits.hits;
        b4 = [];
        c4 = [];
        console.log(a4);
        for (i4 = 0; i4 < a4.length; i4++) {
          b4[i4] = a4[a4.length - 1 - i4]._source.numberValue;
          c4[i4] = set_time(a4[a4.length - 1 - i4]._source.createTime);
        }
        console.log(b4);
        option4.series[0].data = b4;
        option4.xAxis.data = c4;
        //把配置项给实例对象
        myChart4.setOption(option4);
      },
      function (err) {
        console.log(err);
      }
    );
}, 5000);

// ----------------------------------------------------------------
// 第五个折线图，NH3折线图
//初始化 echarts.init(dom容器);

var myChart5 = echarts.init(document.getElementById("chart5"));

//指定配置项与数据
var option5 = {
  color: ["#73c0de"],
  tooltip: {
    trigger: "axis",
  },
  //   网格配置
  grid: {
    left: "0%",
    right: "0%",
    bottom: "3%",
    top: "3%",
    // 是否显示刻度
    containLabel: true,
    show: true, // 显示边框
    borderColor: "#012f4a", // 边框颜色
  },
  xAxis: {
    type: "category",
    data: [
      "Mon",
      "Tue",
      "Wed",
      "Thu",
      "Fri",
      "11",
      "22",
      "33",
      "44",
      "55",
      "66",
      "77",
      "88",
      "99",
      "00",
    ],
  },
  yAxis: {
    type: "value", // 修改分割线
    axisTick: {
      show: false, // 去除刻度
    },
    axisLabel: {
      color: "#dddddd", // 文字颜色
    },
    splitLine: {
      lineStyle: {
        color: "#012f4a", // 分割线颜色
      },
    },
    axisLabel: {
      fontWeight: "bold",
      fontSize: 16,
    },
  },
  series: [
    {
      data: [],
      type: "line",
      smooth: true,
    },
  ],
};
// axios 发起请求获取数据
setInterval(function () {
  axios
    .post(
      "http://42.192.123.52:9200/properties_341623000000011_2022-05/_search?filter_path=hits.hits._source",
      {
        _source: ["createTime", "numberValue"],
        query: {
          bool: {
            must: [{ match: { property: "NH3" } }, { match_all: {} }],
            must_not: [],
            should: [],
          },
        },
        from: 0,
        size: 6,
        sort: [{ createTime: { order: "desc" } }],
        aggs: {},
      }
    )
    .then(
      function (response) {
        a5 = response.data.hits.hits;
        b5 = [];
        c5 = [];
        console.log(a5);
        for (i5 = 0; i5 < a5.length; i5++) {
          b5[i5] = a5[a5.length - 1 - i5]._source.numberValue;
          c5[i5] = set_time(a5[a5.length - 1 - i5]._source.createTime);
        }
        console.log(b5);
        option5.series[0].data = b5;
        option5.xAxis.data = c5;
        //把配置项给实例对象
        myChart5.setOption(option5);
      },
      function (err) {
        console.log(err);
      }
    );
}, 5000);

// 切换静态播放 pig 视频

var changePigVideo = document.getElementById("changePigVideo");
var pigVideo = document.getElementById("pigVideo");
var videoPlay = document.getElementById("videoPlay");
var controlBox = document.getElementById("controlBox");
var clickBox = document.getElementById("clickBox");
var i = 0;
changePigVideo.addEventListener("click", function () {
  if (i % 2 == 0) {
    pigVideo.style.display = "block";
    videoPlay.style.display = "none";
    controlBox.style.display = "none";
    clickBox.style.display = "block";
    changePigVideo.innerHTML="点击切换为实时监控";
  }
  if (i % 2 == 1) {
    videoPlay.style.display = "block";
    pigVideo.style.display = "none";
    controlBox.style.display = "block";
    clickBox.style.display = "none";
    changePigVideo.innerHTML="点击切换为识别与跟踪";
    
  }
  i++;
  console.log(i);
});


// clickBox 点击切换
var pig1_reco= document.getElementById("pig1_reco");
var pig5_reco= document.getElementById("pig5_reco");
var pig1_follow= document.getElementById("pig1_follow");
var pig_video_url=document.getElementById("pig_video_url");

// 判断识别视频是否播放完成，播放完成后，弹出提示框
pig_video_url.addEventListener("ended", function () {

  if(pig_video_url.src=="http://42.192.123.52:12345/follow_pig1_h264.mp4"){

      console.log("1111");
      alert("当前跟踪未发现有异常情况！")
}});


pig1_reco.addEventListener("click", function () {
  pig1_reco.style.background = "#00BBFF";
  pig5_reco.style.background = "#fff";
  pig1_follow.style.background = "#fff";
  pig_video_url.src="http://42.192.123.52:12345/reco_pig1_h264.mp4";

} );

pig5_reco.addEventListener("click", function () {
  pig1_reco.style.background = "#fff";
  pig5_reco.style.background = "#00BBFF";
  pig1_follow.style.background = "#fff";
  pig_video_url.src="http://42.192.123.52:12345/reco_pig5_h264.mp4";
  
} );

pig1_follow.addEventListener("click", function () {
  pig1_reco.style.background = "#fff";
  pig5_reco.style.background = "#fff";
  pig1_follow.style.background = "#00BBFF";
  pig_video_url.src="http://42.192.123.52:12345/follow_pig1_h264.mp4";

} );

