/******
 * @ Author: qiaoyu
 * @ Date: 2022-04-20 21:34:32
* @ LastEditTime: 2022-05-17 10:06:13
* @ LastEditors: qiaoyu
 * @ Description:
* @ FilePath: \jetlinks_big_screen\js\jetlinksApi.js
 * @ Made by qiaoyukeji@gmail.com
 */

// 定义请求唯一凭证 token
var token = "";
sessionStorage.setItem("token", token);

// 发送请求 api 获取 token
function goLogin(userName, passWord) {
  // 发送请求
  axios
    .post("http://42.192.123.52:8848/authorize/login", {
      username: userName,
      password: passWord,
    })
    .then(function (response) {
      token = response.data.result.token;
      
      console.log("token:", token);
      if(token.length>1){
        sessionStorage.setItem("token", token);
        sessionStorage.setItem("userName", userName);
        sessionStorage.setItem("passWord", passWord);
        alert("登录成功");
      }
      return token;
    })
    .catch(function (error) {
      // 用户名密码错误时，http请求就会出错，默认跳到本程序中
      alert("账户名密码错误，登录失败");
      console.log(error);
      return token;
    });
}

// 点击登陆按钮实现登陆功能用已获取 token
var login = document.getElementById("login");
login.addEventListener("click", function () {
  // 判断是否已经获取到 token，若已经获取到 token 则不再获取重复登陆获取
  if (sessionStorage.getItem("token") != "") {
    alert("已登录成功，请勿重复登录");
  } else {
    if (sessionStorage.getItem("userName") && sessionStorage.getItem("passWord")) {
      var userName = sessionStorage.getItem("userName");
      var passWord = sessionStorage.getItem("passWord");
      goLogin(userName, passWord);
      // alert("登录成功");
    } else {
      var userName = prompt("请输入用户名：");
      var passWord = prompt("请输入密码：");
      token = goLogin(userName, passWord);
      // if(token != ""){
      //   sessionStorage.setItem("userName", userName);
      //   sessionStorage.setItem("passWord", passWord);
      //   alert("登录成功");
      // }else{
      //   alert("账户名密码错误，登录失败");
      // }
      
    }
  }
});

// 获取上下左右按钮的位置
var shang = document.getElementById("shang");
var xia = document.getElementById("xia");
var zuo = document.getElementById("zuo");
var you = document.getElementById("you");
// console.log(shang,xia,zuo,you);

// 定义 调用设备功能 api 默认前缀地址
var url =
  "http://42.192.123.52:8848/device/instance/341623000000011001/function";

//   当点击了上下左右按钮的时候，发送请求调用设备功能 上下左右
// 上
shang.addEventListener("click", function () {
  if (sessionStorage.getItem("token") != "") {
    // console.log(shang);
    // 请求 api 获取数据
    axios
      .post(
        url + "/shang",
        {
          functionId: "shang",
        },
        {
          headers: {
            "X-Access-Token": sessionStorage.getItem("token"),
            "Content-Type": "application/json",
          },
        }
      )
      .then(function (response) {
        console.log("----------------------------------------------");
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  } else {
    alert("请先登陆");
  }
});

// xia
xia.addEventListener("click", function () {
  if (sessionStorage.getItem("token") != "") {
    // console.log(xia);
    // 请求 api 获取数据
    axios
      .post(
        url + "/xia",
        {
          functionId: "xia",
        },
        {
          headers: {
            "X-Access-Token": sessionStorage.getItem("token"),
            "Content-Type": "application/json",
          },
        }
      )
      .then(function (response) {
        console.log("----------------------------------------------");
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  } else {
    alert("请先登陆");
  }
});
// zuo
zuo.addEventListener("click", function () {
  if (sessionStorage.getItem("token") != "") {
    // console.log(zuo);
    // 请求 api 获取数据
    axios
      .post(
        url + "/zuo",
        {
          functionId: "zuo",
        },
        {
          headers: {
            "X-Access-Token": sessionStorage.getItem("token"),
            "Content-Type": "application/json",
          },
        }
      )
      .then(function (response) {
        console.log("----------------------------------------------");
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  } else {
    alert("请先登陆");
  }
});
// you
you.addEventListener("click", function () {
  if (sessionStorage.getItem("token") != "") {
    // console.log(you);
    // 请求 api 获取数据
    axios
      .post(
        url + "/you",
        {
          functionId: "you",
        },
        {
          headers: {
            "X-Access-Token": sessionStorage.getItem("token"),
            "Content-Type": "application/json",
          },
        }
      )
      .then(function (response) {
        console.log("----------------------------------------------");
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  } else {
    alert("请先登陆");
  }
});
