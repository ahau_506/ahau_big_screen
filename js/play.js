/******
* @ Author: qiaoyu
* @ Date: 2022-04-17 09:23:35
* @ LastEditTime: 2022-04-27 14:45:04
* @ LastEditors: qiaoyu
* @ Description: 
* @ FilePath: \jetlinks_big_screen\js\play.js
* @ Made by qiaoyukeji@gmail.com
*/
  // 腾讯云 TCplayer 视频播放器
  // var player0 = new TcPlayer("id_test_audio", {
  //   rtmp: "rtmp://42.192.123.52/live/audio", //增加了一个 rtmp 的播放地址，用于PC平台的播放 请替换成实际可用的播放地址
  //   autoplay: true, //iOS 下 safari 浏览器，以及大部分移动端浏览器是不开放视频自动播放这个能力的
  //   width: "640", //视频的显示宽度，请尽量使用视频分辨率宽度
  //   height: "50", //视频的显示高度，请尽量使用视频分辨率高度
  // });


// 腾讯云 TCplayer 视频播放器
var player = new TcPlayer("id_test_video", {
    // "m3u8": "http://42.192.123.52:1936/hls/test/index.m3u8",
    // rtmp: "rtmp://42.192.123.52/live/test", //增加了一个 rtmp 的播放地址，用于PC平台的播放 请替换成实际可用的播放地址
    webrtc: "webrtc://live.qiaoyukeji.cn/live/test", //增加了一个 rtmp 的播放地址，用于PC平台的播放 请替换成实际可用的播放地址
    autoplay: true, //iOS 下 safari 浏览器，以及大部分移动端浏览器是不开放视频自动播放这个能力的
    width: "640", //视频的显示宽度，请尽量使用视频分辨率宽度
    height: "480", //视频的显示高度，请尽量使用视频分辨率高度
    // poster: "http://md.gitnote.cn/1-2021121293535.png", // 封面图
    poster: "https://md.gitnote.cn/jetlinks-1-2021121293535-2022427144431.png", // 封面图
    // webrtcConfig: { streamType: 'auto' },
    pausePosterEnabled: true, // 暂停时的封面是否显示
    // controls:none,// 控件是否显示
    // 定制错误提示语
    wording: {
      2032: "请求视频失败，请检查网络",
      2048: "请求m3u8文件失败，可能是网络错误或者跨域问题",
    },
  });
  

  
  